#include "PracticalSocket.h"
#include <iostream>
#include <string>

int main(){

    const string serverIP = "127.0.0.1";
    unsigned short serverPort = 12345;
    TCPSocket mySocket(serverIP,serverPort);

    int number=15;
    mySocket.send(&number,sizeof(number));

    int result;
    mySocket.recv(&result,sizeof(result));

    std::cout<<"Received from Server:"<<result<<'\n';
    return 0;
}
