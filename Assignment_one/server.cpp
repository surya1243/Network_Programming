#include "PracticalSocket.h"
#include <iostream>
int main(){

    unsigned short port = 12345;
    TCPServerSocket myServerSocket(port);

    TCPSocket * newConnection1 = myServerSocket.accept();
    TCPSocket * newConnection2 = myServerSocket.accept();

    int first,second,sum;

    //obtaining numbers from two client
    newConnection1->recv(&first,sizeof(first));
    newConnection2->recv(&second,sizeof(second));

    //adding those numbers
    sum = first+second;

    //replying sum 
    newConnection1->send(&sum,sizeof(sum));
    newConnection2->send(&sum,sizeof(sum));
    
    return 0;
}
